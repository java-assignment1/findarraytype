package com.tw.arraytype;

import java.util.Scanner;

public class FindArrayType {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter number of Elements:");
        int numberOfElements = scanner.nextInt();

        System.out.println("Enter Array elements");
        int[] arrayElements = new int[numberOfElements];
        for (int index = 0; index < numberOfElements; index++)
            arrayElements[index] = scanner.nextInt();

        int arrayType = getArrayType(numberOfElements, arrayElements);

        if (arrayType == 1)
            System.out.println("ArrayType: Even");
        else if (arrayType == 2)
            System.out.println("ArrayType: Odd");
        else
            System.out.println("ArrayType: Mixed");

    }

    private static int getArrayType(int numberOfElements, int[] arrayElements) {
        int evenNumberCount = 0, oddNumberCount = 0, arrayType = 0;

        for (int index = 0; index < numberOfElements; index++) {
            if (isEven(arrayElements[index]))
                evenNumberCount++;
            else
                oddNumberCount++;
        }

        if (evenNumberCount > 0 && oddNumberCount == 0)
            arrayType = 1;
        else if (evenNumberCount == 0 && oddNumberCount > 0)
            arrayType = 2;
        else
            arrayType = 3;

        return arrayType;
    }

    private static boolean isEven(int arrayElement) {
        return arrayElement % 2 == 0;
    }
}
